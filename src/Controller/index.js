import React, { Component } from 'react'
import './style.css'
import io from 'socket.io-client'
// import RelativeJoycon from '../RelativeJoycon';
import environment from '../environments/environment';
import CustomPrompt from '../CustomPrompt';


class Controller extends Component {
  socket = null
  fullScreenEnabled = false

  state = {
    lobbyId: null,
    nickname: 'Unkown Player',
    prompt: null
  }

  openFullscreen() {
    var elem = document.documentElement;

    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) { /* Firefox */
      elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
      elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE/Edge */
      elem.msRequestFullscreen();
    }
  }

  closeFullscreen() {
    if (document.exitFullscreen) {
      document.exitFullscreen();
    } else if (document.mozCancelFullScreen) { /* Firefox */
      document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
      document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) { /* IE/Edge */
      document.msExitFullscreen();
    }
  }

  componentDidMount() {
    this.socket = io(environment.SERVER_URL)

    this.socket.on('connect', () => {
      this.setState({
        prompt: 'nickname'
      })
    })

    this.socket.on('connectionRefused', () => {
      this.setState({
        prompt: 'lobbyId'
      })
    })
  }

  onNicknameSelected = nickname => {
    this.setState({
      lobbyId: this.props.match.params.lobbyId,
      nickname,
      prompt: null
    })
    this.addController(nickname, this.props.match.params.lobbyId)
  }

  onLobbyIdSelected = lobbyId => {
    this.setState({
      lobbyId,
      prompt: null
    })
    this.addController(this.state.nickname, lobbyId)
  }

  componentWillUnmount() {
    this.socket.off('sync')
    this.socket.off('connectionRefused')
  }

  addController = (nickname, lobbyId) => {
    this.socket.emit('addController', {
      nickname,
      lobbyId
    })
  }

  rotateLeftButtonClickHandler = (event) => {
    event.preventDefault()

    this.socket.emit('rotateLeft')
  }

  rotateRightButtonClickHandler = (event) => {
    event.preventDefault()

    this.socket.emit('rotateRight')
  }

  onJoyconUpdate = (event) => {
    if (event.x < -10) {
      this.socket.emit('moveLeft')
    } else if (event.x > 10) {
      this.socket.emit('moveRight')
    }
  }

  goLeft= () => {
      this.socket.emit('moveLeft')
  }

  goRight= () => {
      this.socket.emit('moveRight')
  }

  rotateLeft= () => {
    this.socket.emit('rotateLeft')
  }

  rotateRight= () => {
    this.socket.emit('rotateRight')
  }

  goDown= () => {
    this.socket.emit('goDown')
  }

  onJoyconDoubleClickStayPressedHandler = () => {
    this.socket.emit('goDown')
  }

  onJoyconDoubleClickHandler = () => {
    this.socket.emit('rotateRight')
  }

  onDoubleClickHandler = (event) => {
    event.stopPropagation();

    if (this.fullScreenEnabled) {
      this.closeFullscreen()
    } else {
      this.openFullscreen()
    }
  }


  render() {
    return (
      <div className="controller" onDoubleClick={this.onDoubleClickHandler}>
        <div className="player-informations">
          <div>{this.state.nickname}</div>
          <div>{this.state.lobbyId ? this.state.lobbyId : ''}</div>
        </div>
        {this.state.prompt && this.state.prompt === 'nickname' && (
          <CustomPrompt title="Please set your nickname !" onConfirm={this.onNicknameSelected}/>
        )}
        {this.state.prompt && this.state.prompt === 'lobbyId' && (
          <CustomPrompt
            title="Please set the lobby code !"
            onConfirm={this.onLobbyIdSelected} />
        )}
        {!this.state.prompt && (
          <div className="directional-buttons">
            <div className="left-panel">
              <button onClick={this.goLeft}>Move</button>
              <button onClick={this.goDown}>Down</button>
            </div>
            <div className="spacer"></div>
            <div className="right-panel">
              <button onClick={this.goRight}>Move</button>
              <button onClick={this.rotateRight}>Rotate</button>
            </div>
            {/*<RelativeJoycon*/}
            {/*  update={this.onJoyconUpdate}*/}
            {/*  onDoubleClickStayPressed={this.onJoyconDoubleClickStayPressedHandler}*/}
            {/*  onDoubleClick={this.onJoyconDoubleClickHandler} />*/}
          </div>
        )}
      </div>
    )
  }
}

export default Controller
