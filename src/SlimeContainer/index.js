import React, { Component } from "react"
import Slime from "../Slime"

import './style.css'

class SlimeContainer extends Component {
  getEnemySlimesWrapper = () => {
    if (!this.props.enemySlimes || this.props.enemySlimes === 0) {
      return <div className="next-enemies"></div>
    } else {
      // let bigOnes = Math.floor(this.props.enemySlimes / this.props.board.slimes[0].length)
      // let smallOnes = this.props.enemySlimes - bigOnes * this.props.board.slimes[0].length
      let bigOnes = Math.floor(this.props.enemySlimes / 8)
      let smallOnes = this.props.enemySlimes - bigOnes - 8
      return (
        <div className="next-enemies">
          {bigOnes !== 0 &&
            <Slime
              color={0}
              animated={false}
              moving={false}
              width="75px"
              marginRight="15px"
              merge={bigOnes} />
          }
          {smallOnes !== 0 &&
            <Slime
              color={0}
              animated={false}
              moving={false}
              width="50px"
              merge={smallOnes} />
          }
        </div>
      )
    }
  }

  render() {
    return (
      <div className="slime-container">
        <div className="board">
          {this.props.lost && <div className="win-lose">{this.props.name} lost</div>}
          {this.props.won && <div className="win-lose">{this.props.name} won</div>}
          {this.props.board && this.props.board.slimes && this.props.board.slimes.map((slimeRow, rowIndex) =>
            slimeRow.map((slimeColumn, columnIndex) =>
              <Slime
                color={slimeColumn}
                animated={this.props.board.animated && this.props.board.animated.indexOf(rowIndex * slimeRow.length + columnIndex) !== -1}
                moving={this.props.board.moving && this.props.board.moving.indexOf(rowIndex * slimeRow.length + columnIndex) !== -1}
                key={rowIndex * slimeRow.length + columnIndex} />
            )
          )}
        </div>
        <div className="informations">
          <div style={{ textAlign: 'center', textTransform: 'uppercase' }}>{this.props.name ? this.props.name : 'Unkown Player'}</div>
          <div className="next-couple">
            {this.props.nextCouple && this.props.nextCouple.map((color, index) =>
              <Slime
                color={color}
                animated={false}
                moving={false}
                width="50px"
                key={index} />
            )}
          </div>
          {this.getEnemySlimesWrapper()}
        </div>
      </div>
    )
  }
}

export default SlimeContainer
