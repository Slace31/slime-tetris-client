import React, { Component } from 'react'
import './style.css'

class RelativeJoycon extends Component {
  state = {
    initialPosition: null,
    actualPosition: null
  }

  wrapper = null
  interval = null
  intervalDoubeClickStay = null
  touchStartTime = null
  touchEndTime = null
  lastClickTime = null

  onTouchStartHandler = (event) => {
    event.preventDefault()
    if (event.touches) {
      event = event.touches[0]
    }

    this.setState({
      initialPosition: {
        x: event.clientX,
        y: event.clientY
      },
      actualPosition: {
        x: event.clientX,
        y: event.clientY
      }
    })

    if (this.lastClickTime && this.lastClickTime <= 100 && this.touchEndTime - this.touchStartTime < 100) {
      this.intervalDoubeClickStay = window.setInterval(() => {
        if (this.props.onDoubleClickStayPressed && Date.now() - this.touchStartTime > 150) {
          this.lastClickTime = null
          this.props.onDoubleClickStayPressed()
        }
      }, 100)
    } else {
      this.interval = window.setInterval(() => {
        this.update()
      }, 100)
    }

    this.touchStartTime = Date.now()
  }

  update() {
    if (this.props.update && this.state.initialPosition && this.state.actualPosition) {
      this.props.update({
        x: (this.state.initialPosition.x - this.state.actualPosition.x) / this.wrapper.offsetWidth * -100,
        y: (this.state.initialPosition.y - this.state.actualPosition.y) / this.wrapper.offsetHeight * -100
      })
    }
  }

  onTouchMoveHandler = (event) => {
    event.preventDefault()

    if (event.touches) {
      event = event.touches[0]
    }

    if (this.state.initialPosition && this.state.actualPosition) {
      this.setState({
        actualPosition: {
          x: event.clientX,
          y: event.clientY
        }
      })
    }
  }

  onTouchEndHandler = (event) => {
    event.preventDefault()

    this.update()

    this.setState({
      initialPosition: null,
      actualPosition: null
    })

    this.touchEndTime = Date.now()
    if (this.lastClickTime && this.lastClickTime <= 100 && this.touchEndTime - this.touchStartTime < 100) {
      if (this.props.onDoubleClick) {
        this.props.onDoubleClick()
      }
      this.lastClickTime = null
    } else {
      this.lastClickTime = this.touchEndTime - this.touchStartTime
    }

    window.clearInterval(this.intervalDoubeClickStay)
    window.clearInterval(this.interval)
  }

  get isMobileDevice() {
    return 'ontouchstart' in document.documentElement
  }

  render() {
    const { initialPosition, actualPosition } = this.state

    return (
      <div
        className="relative-joycon"
        ref={wrapper => this.wrapper = wrapper}
        onTouchStart={this.onTouchStartHandler}
        onMouseDown={!this.isMobileDevice && this.onTouchStartHandler}
        onTouchEnd={this.onTouchEndHandler}
        onMouseUp={!this.isMobileDevice && this.onTouchEndHandler}
        onTouchMove={this.onTouchMoveHandler}
        onMouseMove={!this.isMobileDevice && this.onTouchMoveHandler} >
        {initialPosition && <div className="initial-position" style={{ top: initialPosition.y + 'px', left: initialPosition.x + 'px' }}></div>}
        {actualPosition && <div className="actual-position" style={{ top: actualPosition.y + 'px', left: actualPosition.x + 'px' }}></div>}
      </div>
    )
  }
}

export default RelativeJoycon