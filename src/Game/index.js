import React, { Component } from 'react'
import SlimeContainer from '../SlimeContainer'
import io from 'socket.io-client'
import './style.css'
import environment from '../environments/environment'
import { Redirect } from 'react-router-dom'

class Game extends Component {
  socket = null
  state = {
    players: [{
      id: '1',
      score: 0,
      lockedTicksCount: 0,
      nextAction: null,
      enemySlimes: 17,
      nextCouple: [1,1],
      actualCouple: null,
      moving: [],
      destroying: [],
      combo: [],
      board: [
        [-1, -1, -1, -1, -1, -1, -1, -1],
        [-1, -1, -1, -1, -1, -1, -1, -1],
        [-1, -1, -1, -1, -1, -1, -1, -1],
        [-1, -1, -1, -1, -1, -1, -1, -1],
        [-1, -1, -1, -1, -1, -1, -1, -1],
        [-1, -1, -1, -1, -1, -1, -1, -1],
        [-1, -1, -1, -1, -1, -1, -1, -1],
        [-1, -1, -1, -1, -1, -1, -1, -1],
        [-1, -1, -1, -1, -1, -1, -1, -1],
        [-1, -1, -1, -1, -1, -1, -1, -1],
        [-1, -1, -1, -1, -1, -1, -1, -1],
        [-1, -1, -1, -1, -1, -1, -1, -1],
        [-1, -1, -1, -1, -1, -1, -1, -1],
        [-1, -1, -1, -1, -1, -1, -1, -1],
        [-1, -1, -1, -1, -1, -1, -1, -1],
      ]
    }, null],
    loser: null,
    redirect: null,
    gameActiveOverlay: {
      show: false,
      dismissed: false
    },
    gamePendingOverlay: false
  }

  timeout = null

  componentDidMount() {
    this.socket = io(environment.SERVER_URL)

    this.socket.on('connect', () => {
      this.socket.emit('addScreen', {
        lobbyId: this.props.match.params.lobbyId
      })
    })


    this.socket.on('sync', players => {
      this.setState({
        players,
        loser: null
      })
    })

    this.socket.on('gameEnd', message => {
      let { loser } = message
      this.setState({
        loser
      })
    })

    this.timeout = window.setTimeout(() => {
      if(!this.state.gameActiveOverlay.show) {
        this.setState({
          gamePendingOverlay: true,
        })
      }
    }, 500)
  }

  componentWillUnmount() {
    this.socket.off('sync')
    this.socket.off('connect')
  }

  componentDidUpdate() {
    if (this.state.players[1] && !this.state.gameActiveOverlay.show && !this.state.gameActiveOverlay.dismissed) {
      if(this.state.gamePendingOverlay) {
        this.setState({
          gamePendingOverlay: false,
          gameActiveOverlay: {
            show: true,
            dismissed: true
          },
        })
      } else {
        this.setState({
          gamePendingOverlay: false,
          gameActiveOverlay: {
            show: true,
            dismissed: false
          },
        })
      }
    } else if (this.state.players[0] === null && !this.state.gamePendingOverlay) {
      this.setState({
        gamePendingOverlay: true,
        gameActiveOverlay: {
          show: false,
          dismissed: true
        },
      })
    }
  }

  spectate = () => {
    this.setState({
      gameActiveOverlay: {
        show: true,
        dismissed: true
      },
    })
  }

  createLobby = () => {
    window.location.href = window.location.origin + '/lobby/' + Date.now()
  }

  render() {
    if (window.innerWidth < 768) {
      return <Redirect to="/controller" />
    }

    return (
      <div className="game">
        {this.state.gameActiveOverlay.show && !this.state.gameActiveOverlay.dismissed && (
          <div className="overlay">
            <img src="/images/brand-transparent-bg.png" alt="Slime Fusion" className="brand" />
            <div className="info">
              <div>Sorry, but there is already two competitive players in this lobby..</div>
              <div>If you want to beat your friend you may create a new lobby, but you can still spectate this genious players !</div>
            </div>
            <div className="buttons">
              <button onClick={this.spectate}>Spectate</button>
              <button onClick={this.createLobby}>Create lobby</button>
            </div>
          </div>
        )}
        {this.state.gamePendingOverlay && (
          <div className="overlay">
            <img src="/images/brand-transparent-bg.png" alt="Slime Fusion" className="brand" />
            <div className="info">
              {this.props.match.params.lobbyId && <div><b>Lobby code:</b> {this.props.match.params.lobbyId}<br/></div>}
              <div>The game is not started yet..</div>
              <div>To connect a controller, start Slime Fusion on your phone or tablet !</div>
              <div>The game will start three seconds when two controllers are ready.</div>
              <br/>
              {this.state.players[0] !== null && <div>{this.state.players[0].name} is connected</div>}
            </div>
          </div>
        )}
        <SlimeContainer
          {...this.state.players[0]}
          lost={this.state.players[0] && this.state.players[0].id === this.state.loser}
          won={this.state.loser && this.state.players[0] && this.state.players[0].id !== this.state.loser} />
        <SlimeContainer
          {...this.state.players[1]}
          lost={this.state.players[1] && this.state.players[1].id === this.state.loser}
          won={this.state.loser && this.state.players[1] && this.state.players[1].id !== this.state.loser} />
      </div>
    )
  }
}

export default Game
