import React, { Component } from 'react';
import './App.css';
import { Switch, Route } from 'react-router-dom'
import Controller from './Controller';
import Game from './Game';

class App extends Component {
  state = {
    showApp: false
  }

  componentDidMount() {
    window.setTimeout(() => {
      this.setState({
        showApp: true
      })
    }, 0)
  }

  render() {
    return (
      <div className="App">
        {this.state.showApp && (<Switch>
          <Route path="/controller/:lobbyId" component={Controller} />
          <Route path="/controller" component={Controller} />
          <Route path="/lobby/:lobbyId" component={Game} />
          <Route component={Game} />
        </Switch>)}
      </div>
    );
  }
}

export default App;