import React, { Component } from "react"
import './style.css'

class Slime extends Component {
  static defaultProps = {
    color: -1,
    animated: false,
    moving: false,
    width: '12.5%',
    marginRight: '0px',
    merge: 0,
  }

  getColor(color) {
    switch (color) {
      case 0:
        return "grey"
      case 1:
        return "red"
      case 2:
        return "blue"
      case 3:
        return "green"
      case 4:
        return "yellow"
      default:
        return null
    }
  }

  render() {
    return (
      <div
        className={`slime${this.props.animated ? ' animated' : ''}${this.props.moving && !this.props.animated ? ' moving' : ''}`}
        style={{
          backgroundImage: this.getColor(this.props.color) && `url(/images/slimes/${this.getColor(this.props.color)}.png)`,
          width: this.props.width,
          marginRight: this.props.marginRight
        }} >
        {this.props.merge > 1 && <div className="pin">{this.props.merge}</div>}
      </div>
    )
  }
}

export default Slime
