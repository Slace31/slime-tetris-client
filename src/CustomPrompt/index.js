import React, { Component } from 'react'
import './style.css'

class CustomPrompt extends Component {
    state = {
        value: ''
    }

    handleChange = event => {
        const { value } = event.target
        this.setState({
            value
        })
    }

    render() {
        return (
            <div className="custom-prompt">
                <div>{this.props.title}</div>
                <input type="text" value={this.state.value} onChange={this.handleChange} />
                <div className="buttons">
                    <button disabled={this.state.value.length < 3} onClick={() => this.props.onConfirm(this.state.value)}>OK</button>
                </div>
            </div>
        )
    }
}

export default CustomPrompt